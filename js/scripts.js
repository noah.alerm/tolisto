'use strict'

//URL
const API_URL = 'https://tolistoapi.herokuapp.com'

//CURRENT LIST
let currentListID = 0

//REQUESTS CONSTs
const requestPosition = new XMLHttpRequest()
const requestTasks = new XMLHttpRequest()
const requestLists = new XMLHttpRequest()
const requestTaskPost = new XMLHttpRequest()
const requestListPost = new XMLHttpRequest()
const requestTaskDelete = new XMLHttpRequest()
const requestListDelete = new XMLHttpRequest()
const requestListPut = new XMLHttpRequest()




//------------- GET -------------//

//LIST REQUEST
function onListRequestHandler() {
    console.log("Successful connection to API lists")
    const data = JSON.parse(this.response)
    console.log(data);

    data.forEach(list =>{
        //TASK
        let ul = document.querySelector('#lists');
        let li = document.createElement("li");
        let deleteButton= document.createElement("button")

        li.append(list.name);
        li.setAttribute("id", list.listId);
        li.setAttribute("class","List");
        li.setAttribute("onclick","getTasks(this.id)");
        deleteButton.setAttribute("type", "submit")
        deleteButton.setAttribute("class", "delButtonList")
        deleteButton.setAttribute("onclick", "deleteList(this.parentNode.id)")
        deleteButton.append("\u00D7")

        li.append(deleteButton);
        ul.append(li);
    })
    let ul = document.querySelector('#lists');
    let li = document.createElement("li");

    li.append("ADD LIST")
    li.setAttribute("class","List");
    li.setAttribute("onClick", "getValueList()")

    li.style.backgroundColor = "#81a7e3"
    li.style.marginTop = "40px"

    ul.append(li)
}

requestLists.addEventListener('load', onListRequestHandler)
requestLists.open('GET', API_URL+"/lists")
requestLists.send()



/**
 * This method is used to refresh the lists.
 */
function getLists() {
    document.querySelector('#lists').innerHTML = "";
    onListRequestHandler()
    requestLists.open('GET', API_URL+"/lists")
    requestLists.send()
}


//TASK REQUEST
function onTaskRequestHandler() {
        console.log("Successful connection to API tasks")
        const data = JSON.parse(this.response)
        console.log(data);

    data.forEach(task =>{
        //TASK
        let ul = document.querySelector('#divTask');
        let li = document.createElement("li");
        let check = document.createElement("input");
        let deleteButton= document.createElement("button")
        //CHECK IF DONE
        if (task.done)
        check.checked = true

        li.append(task.description);
        li.setAttribute("id", task.id);
        li.setAttribute("description",task.description)
        li.setAttribute("done",task.done)
        li.setAttribute("position",task.position)
        ul.append(li);
        li.append(check);
        check.setAttribute("type","checkbox");
        check.setAttribute("onclick", "putTask(this.parentNode.id)")
        li.setAttribute("class","tarea");
        deleteButton.setAttribute("type", "submit")
        deleteButton.setAttribute("class", "delButton")
        deleteButton.setAttribute("onclick", "deleteTask(this.parentNode)")
        deleteButton.append("\u00D7")
        li.append(deleteButton);
    });
}

/**
 * This method is used to get the tasks from the API when a list is clicked.
 * @param idList List's ID
 * @see onListRequestHandler()
 */
function getTasks(idList) {

    //CURRENT LIST UPDATE
    currentListID = idList;

    document.querySelector('#main').style.display="block";

    document.querySelector('#divTask').innerHTML = "";

    document.getElementById("listName").innerText = document.getElementById(idList).firstChild.nodeValue

    let h2 = document.querySelector('#listName');
    let button = document.createElement("button");

    button.append("EDIT NAME")
    button.setAttribute("class", "editListButton")
    button.setAttribute("onClick", "putList()")
    h2.append(button)
    requestTasks.addEventListener('load', onTaskRequestHandler);
    requestTasks.open('GET',API_URL+"/lists/"+idList+"/tasks");
    requestTasks.send();

}



//------------- POST -------------//

//TASK
function getValueTask() {
    let name = prompt("Enter the task name: ", "the name here");

    let li = document.getElementById("divTask").getElementsByTagName("li");

    if (name !== null) {
        requestTaskPost.open("POST", API_URL + "/lists/" + currentListID + "/tasks", true);
        requestTaskPost.setRequestHeader('Content-Type', 'application/json');
        requestTaskPost.send(JSON.stringify({
            description: name,
            done: false,
            position: li.length
        }));
    }

    setTimeout(() => { document.getElementById(currentListID).click(); }, 200);
}


//LIST
function getValueList() {
    let name = prompt("Enter the name's list : ", "the name here");
    if (name !== null){
        requestListPost.open("POST", API_URL+"/lists/", true);
        requestListPost.setRequestHeader('Content-Type', 'application/json');
        requestListPost.send(JSON.stringify({
            name: name
        }));
    }

    setTimeout(() => { window.location.reload(); }, 200);
}



//------------- DELETE -------------//

//TASK
function reorderPosition(task){
    let li = document.getElementById("divTask").getElementsByTagName("li");

    for (let i = 0; i < li.length ; i++) {
        if(li[i].getAttribute('position') > task.getAttribute('position')){

            requestPosition.open("PUT", API_URL+"/lists/"+currentListID+"/tasks/"+li[i].id, true);
            requestPosition.setRequestHeader('Content-Type', 'application/json');
            requestPosition.send(JSON.stringify({
                id: li[i].id,
                description: li[i].getAttribute('description'),
                done: li[i].getAttribute('done'),
                position: li[i].getAttribute('position')-1
            }));
        }
    }
}

function deleteTask(task) {
    console.log("prova: " + task)

    requestTaskDelete.open("DELETE", API_URL+"/lists/"+currentListID+"/tasks/"+task.id, true);
    requestTaskDelete.setRequestHeader('Content-Type', 'application/json');
    requestTaskDelete.send();

    //reorderPosition(task)

    setTimeout(() => { document.getElementById(currentListID).click(); }, 200);
}

//LIST
function deleteList(id) {
    requestListDelete.open("DELETE", API_URL+"/lists/"+id, true);
    requestListDelete.setRequestHeader('Content-Type', 'application/json');
    requestListDelete.send();

    setTimeout(() => { window.location.reload();},200)
}




//------------- PUT -------------//

//TASK
function putTask(id) {
    requestTaskPost.open("PUT", API_URL+"/lists/"+currentListID+"/tasks/"+id, true);
    requestTaskPost.send();
}

function putList() {
    let name = prompt("Enter the NEW name:", "List Name")

    if (name !== null) {
        requestListPut.open("PUT", API_URL + "/lists/" + currentListID, true);
        requestListPut.setRequestHeader('Content-Type', 'application/json');
        requestListPut.send(JSON.stringify({
            listId: currentListID,
            name: name
        }));
    }

    setTimeout(() => { document.getElementById(currentListID).click(); }, 200);
}


/**
 * This method modifies CORS to access from LOCALHOST to HEROKU DEPLOYMENT
 */
function getItems(){
    let end_point = 'https://tolistoapi.herokuapp.com/lists';

    fetch(end_point)
        .then (response => response.json())
        .then(data => console.log(data))

}
/*LIST VISIBILITY*/
function listVisibility() {
    let elements = document.getElementsByClassName('List');

    Array.from(elements).forEach((x) =>{
        if(x.style.display ==="none" || x.style.display ===""){
            x.style.display = "block";
        }else{
            x.style.display = "none";
        }
    })
}

/*DRAG & DROP*/
const lista = document.getElementById("divTask");
Sortable.create(lista,{})

